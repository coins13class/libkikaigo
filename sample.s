	    .text
	    .align 4
	    .globl main
main:
	    mov $1, %eax
	    mov $2, %ebx
	    mov $3, %ecx
	    mov $4, %edx
	    call break   # eax=1, ebx=2, ecx=3, edx=4
	    add $1, %eax
	    call break   # eax=2, ebx=2, ecx=3, edx=4
	    add $2, %ebx 
	    add $3, %ecx
	    add $4, %edx
	    call stop    # eax=2, ebx=4, ecx=6, edx=8
