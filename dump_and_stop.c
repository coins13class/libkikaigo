#include <stdio.h>
#include <stdlib.h>
void dump_and_stop(int edi, int esi, int edx, int ecx, int ebx, int eax, int ebp, int pc, int dummy)
{
  printf("stop at pc=0x%x ... \n", pc);
  printf("eax=0x%08x, ebx=0x%08x, ecx=0x%08x, edx=0x%08x\n", eax, ebx, ecx, edx);
  printf("esi=0x%08x, edi=0x%08x, ebp=0x%08x, esp=0x%08x\n", esi, edi, ebp, (int)(&dummy));
  exit(0);
}
