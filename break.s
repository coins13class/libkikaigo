	    .text
STR:    .string "break at ... \neax=0x%08x, ebx=0x%08x, ecx=0x%08x, edx=0x%08x\n"
	    .align  4
	    .globl break
break:
	    push %edx        
	    push %ecx
	    push %ebx
	    push %eax
	    push $STR
	    call printf
	    pop %eax # $STRをstackから取り出す
	    pop %eax
	    pop %ebx
	    pop %ecx
	    pop %edx
	    ret
