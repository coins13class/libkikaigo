AR = ar
CC = cc
CFLAGS = -Wall -m32
ASFLAGS = -Wall -m32
OBJS = dump_and_stop.o stop.o break.o
TARGET = libkikaigo.a

$(TARGET): $(OBJS)
	$(AR) cr $(TARGET) $^

.c.o:
	$(CC) $(CFLAGS) -c $<
.s.o:
	$(CC) $(ASFLAGS) -c $<

run-sample:
	$(CC) $(CFLAGS) sample.s $(TARGET) -o sample
	./sample
	$(RM) sample

.PHONY: clean
clean:
	$(RM) $(OBJS) $(TARGET)
