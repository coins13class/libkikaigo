# libkikaigo-clone
システムプログラミング序論で用いられる```libkikaigo.a```を自分の環境でも利用できるようにするためのプロジェクト

出力結果としては各レジスタとスタックポインタなどを表示する.

**現状OSXでは動きません** おとなしくLinuxを用意してください.

## breakを追加しました
breakの機能を追加しました.これでeax, ebx, ecx, edxレジスタの各値が標準出力に出力されます.

使い方はstopと同じで出力したいところでcall breakとします.これを使えばGDBは不要かな?

## 結果
sample.sの実行結果.

```
$ make run-sample
cc -Wall -m32 sample.s libkikaigo.a -o sample
./sample
break at ...
eax=0x00000001, ebx=0x00000002, ecx=0x00000003, edx=0x00000004
break at ...
eax=0x00000002, ebx=0x00000002, ecx=0x00000003, edx=0x00000004
stop at pc=0x804847f ...
eax=0x00000002, ebx=0x00000004, ecx=0x00000006, edx=0x00000008
esi=0x00000000, edi=0x00000000, ebp=0x00000000, esp=0xffe696ec
rm -f sample
```

## libkikaigo.aを自分の環境で生み出す方法
リポジトリを適当な場所にクローンしてmakeすればよい.

```
$ git clone https://bitbucket.org/coins13class/libkikaigo.git
$ cd /path/to/libkikaigo
$ make
```

もしもUbuntu 64bitのmakeでコケる場合以下の理由があるので確認されたし.

## 各環境への対応状況
### pentas-comp[a-f]
普通に動く.

### Ubuntu 14.04 LTS
amd64でインストールしているとlibcにi386用の物が入っておらずmakeがこける.

そこでi386用のlibcを入れる.

```$ sudo apt-get install libc6-dev-i386```

これで再度makeで動くはず

### 他のディストリビューション
もしもOSをインストールした時にx86_64(amd64)版でインストールした場合は適当にlibcのi386版を入れてください.

## OSX
謎.わからん.なんかldでこける.強い人助けて

多分,10.9以降はlibc 32bit版がそもそも混入されていないのかもしれない.
